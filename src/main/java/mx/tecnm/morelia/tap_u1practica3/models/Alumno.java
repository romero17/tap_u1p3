
package mx.tecnm.morelia.tap_u1practica3.models;

import java.io.Serializable;

/**
 *
 * @author  Andrea
 */
public class Alumno implements Serializable{
    private String numControl;
    private String nombre;
    private String carrera;
    private int semestre;
    
    public Alumno(){
        
    }
    
    public Alumno(String numControl, String nombre, String carrera, int semestre) {
        this.numControl = numControl;
        this.nombre = nombre;
        this.carrera = carrera;
        this.semestre = semestre;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    public String getNumControl() {
        return numControl;
    }

    public void setNumControl(String numControl) {
        this.numControl = numControl;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    
}
