
package mx.tecnm.morelia.tap_u1practica3.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import mx.tecnm.morelia.tap_u1practica3.models.Alumno;


/**
 *
 * @author Andrea
 */
public class Tools {
    //lee la lista de objetos
    public static ArrayList<Alumno> leerListaAlumnos(File archivo) {
        ArrayList<Alumno> alumnos = new ArrayList<>();
        
        try {
            FileInputStream fis = new FileInputStream(archivo);
            ObjectInputStream ois = new ObjectInputStream(fis);
            alumnos = (ArrayList<Alumno>)ois.readObject();
            ois.close();
                    
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        //Mostrar en consola los datos del alumno
        
        for (Alumno alumno : alumnos) {
            System.out.println(alumno.getNumControl());
            System.out.println(alumno.getNombre());
            System.out.println(alumno.getCarrera());
            System.out.println(""+alumno.getSemestre());

        }
        return alumnos;    
    }
            
}
